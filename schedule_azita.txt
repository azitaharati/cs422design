setup initial Android project and commit to BitBucket -- Thu 03/13 by 5pm (estimate: 2 hrs)
design Login screen -- Sat 03/14 by 10am (estimate: 1 hr)
implement Login flow with DB connection -- Sat 03/14 by 1pm (estimate: 3 hr)
implement Logout flow -- Sat 03/14 by 4pm (estimate: 2 hrs)
design Insert Data page - Sun 03/15 by 1pm (estimate: 2 hr)
implement Insert Data flow -- Sun 03/15 by 4pm (estimate: 3 hrs)
design phase 1 version of View page - Sun 03/15 by 6pm (estimate: 2 hr)
smoke screen testing - Sun 03/15 by 8pm (estimate: 2 hr)

March 15 - All above tasks have been completed, repository updated, and tasks resolved in Bitbucket.
