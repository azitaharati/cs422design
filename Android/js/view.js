var desktopCount = 0;
var laptopCount = 0;
$(function() {
	if (!isMobileDevice) {
		var thead = "\
								<th>COMP NAME</th>\
								<th>SERIAL</th>\
								<th>PTAG</th>\
								<th>USER</th>\
								<th>LOCATION</th>\
								<th>MODEL</th>\
								<th>PURCHASED</th>\
								<th>AGE</th>\
								<th>P/S/H</th>\
								<th>DEPT</th>\
								<th>CC</th>\
		";
		$("#desktopTable thead tr").html(thead);
		$("#laptopTable thead tr").html(thead);

		$("#pagesize").val(10);
	}
	var Device = Parse.Object.extend("Device");
	var query = new Parse.Query(Device);
	query.find({
		success: function(results) {
			var desktopTable = "";
			var laptopTable = "";
			for (i = 0; i < results.length; i++) {
				var attributes = results[i].attributes;
				if (attributes.type == "D") {
					desktopCount++;
					tableId = "desktopTable";
					if (isMobileDevice) {
						desktopTable = desktopTable + "\
								<tr onclick=\"view('" + results[i].id + "')\">\
									<td>" + attributes.compName + "</td>\
									<td>" + attributes.userLastName + ", " + attributes.userFirstName + "</td>\
								</tr>";
					} else {
						desktopTable = desktopTable + "\
								<tr onclick=\"view('" + results[i].id + "')\">\
									<td>" + attributes.compName + "</td>\
									<td>" + attributes.serialNo + "</td>\
									<td>" + attributes.ptag + "</td>\
									<td>" + attributes.userLastName + ", " + attributes.userFirstName + "</td>\
									<td>" + attributes.location + "</td>\
									<td>" + attributes.model + "</td>\
									<td>" + attributes.purchased + "</td>\
									<td>" + attributes.age + "</td>\
									<td>" + attributes.psh + "</td>\
									<td>" + attributes.dept + "</td>\
									<td>" + attributes.cc + "</td>\
								</tr>";

					}
				} else {
					laptopCount++;
					tableId = "laptopTable";
					if (isMobileDevice) {
						laptopTable = laptopTable + "\
								<tr onclick=\"view('" + results[i].id + "')\">\
									<td>" + attributes.compName + "</td>\
									<td>" + attributes.userLastName + ", " + attributes.userFirstName + "</td>\
								</tr>";
					} else {
						laptopTable = laptopTable + "\
								<tr onclick=\"view('" + results[i].id + "')\">\
									<td>" + attributes.compName + "</td>\
									<td>" + attributes.serialNo + "</td>\
									<td>" + attributes.ptag + "</td>\
									<td>" + attributes.userLastName + ", " + attributes.userFirstName + "</td>\
									<td>" + attributes.location + "</td>\
									<td>" + attributes.model + "</td>\
									<td>" + attributes.purchased + "</td>\
									<td>" + attributes.age + "</td>\
									<td>" + attributes.psh + "</td>\
									<td>" + attributes.dept + "</td>\
									<td>" + attributes.cc + "</td>\
								</tr>";
					}
				}
			}
			$("#desktopCount").html(desktopCount);
			$("#laptopCount").html(laptopCount);

			if (desktopCount == 0) {
				$("#desktopNoResults").show();
				$("#desktopTable").hide();
				$("#desktopPager").hide();
			} else
				$("#desktopTable tbody").html(desktopTable);

			if (laptopCount == 0) {
				$("#laptopNoResults").show();
				$("#laptopTable").hide();
				$("#laptopPager").hide();
			} else
				$("#laptopTable tbody").html(laptopTable);

			loadTableSorter();

			loadAccordian();
		},
		error: function(error) {
			LogException("Error: " + error.code + " " + error.message);
		}
	});

	$("#pagesize").change(function() {
		var pageSize = parseInt($(this).val());
		if (desktopCount > 0) $("#desktopTable").trigger("pageAndSize", [1, pageSize]);
		if (laptopCount > 0) $("#laptopTable").trigger("pageAndSize", [1, pageSize]);
	});
});

function loadTableSorter() {
	var pageSize = parseInt($("#pagesize").val());
	if (desktopCount > 0)
		$("#desktopTable")
			.tablesorter({widthFixed: true, widgets: ['zebra'], theme: 'default', sortList: [[0, 0]]})
			.tablesorterPager({container: $("#desktopPager")})
			.trigger("pageAndSize", [1, pageSize]);
	if (laptopCount > 0)
		$("#laptopTable")
			.tablesorter({widthFixed: true, widgets: ['zebra'], theme: 'default', sortList: [[0, 0]]})
			.tablesorterPager({container: $("#laptopPager")})
			.trigger("pageAndSize", [1, pageSize]);
}

function loadAccordian() {
	$("#accordion").accordion({
		collapsible: true,
		autoHeight: true,
		heightStyle: "content"
	});
}

function view(objectId) {
	redirectTo("detail.html?id=" + objectId);
}