$(function() {
	var objectId = getParameterByName("id");
	var Device = Parse.Object.extend("Device");
	var query = new Parse.Query(Device);
	query.equalTo("objectId", objectId);
	query.find({
		success: function(results) {
			if (results.length == 0) {
				alert("Device not found.");
			} else {
				var attributes = results[0].attributes;
				$("#compName").val(attributes.compName);
				$("#serialNo").val(attributes.serialNo);
				$("#ptag").val(attributes.ptag);
				$("#user").val(attributes.userLastName + ", " + attributes.userFirstName);
				$("#location").val(attributes.location);
				$("#model").val(attributes.model);
				$("#purchased").val(attributes.purchased);
				$("#age").val(attributes.age);
				$("#psh").val(attributes.psh);
				$("#dept").val(attributes.dept);
				$("#cc").val(attributes.cc);
				$("#type").val(attributes.type == "d" ? "Desktop" : "Laptop");
			}
		},
		error: function(error) {
			LogException("Error: " + error.code + " " + error.message);
		}
	});

	$("#editBtn").click(function() {
		var objectId = getParameterByName("id");
		redirectTo("update.html?id=" + objectId);
	});
	
	$("#deleteBtn").click(function() {
		if (confirm("You are about to perform an action that cannot be reverted.  Are you sure you want to delete this device record?")) {
			var objectId = getParameterByName("id");
			var device = new Device();
			device.id = objectId;
			device.destroy({
				success: function(myObject) {
					redirectTo("view.html");
				},
				error: function(myObject, error) {
					LogException("Error: " + error.code + " " + error.message);
				}
			});
		}
	});
});
