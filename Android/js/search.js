var comparison = {
  contains : 1,
  exact : 2,
  startsWith : 3,
  before : 4,
  after : 5,
  lessThan : 6,
  greaterThan : 7
};

var comparisonDescription = [
  "containing",
  "equal to",
  "starting with",
  "before",
  "after",
  "less than",
  "greater than"
];

var prevSearchType = "";

$(function() {
	$('.slideout-menu-toggle').on('click', function(event) {
  	event.preventDefault();
  	var slideoutMenu = $('.slideout-menu');
  	var slideoutMenuWidth = $('.slideout-menu').width();
  	
  	slideoutMenu.toggleClass("open");
  	
  	if (slideoutMenu.hasClass("open")) {
    	slideoutMenu.animate({
	    	left: "0px"
    	});	
  	} else {
    	slideoutMenu.animate({
	    	left: -slideoutMenuWidth
    	}, 250);	
  	}
  });
  $("#saveSettings").click(function() {
  	if ($("#filterField").val() == "purchased") {
      $("#keyword").datepicker();
    } else {
      $("#keyword").datepicker("destroy").removeClass("hasDatepicker");
    }
  	$('.slideout-menu-toggle').click();
  });
  $("#filterField").change(function() {
  	var type = $(this).find("option:selected").attr("type");
  	switch(type) {
  		case "string":
  			$("#filterComparison").html("\
                <option value=\"" + comparison.contains + "\">contains</option>\
                <option value=\"" + comparison.exact + "\">exact</option>\
                <option value=\"" + comparison.startsWith + "\">starts with</option>\
  				");
  			break;
  		case "date":
  			$("#filterComparison").html("\
                <option value=\"" + comparison.exact + "\">exact</option>\
                <option value=\"" + comparison.before + "\">before</option>\
                <option value=\"" + comparison.after + "\">after</option>\
  				");
  			break;
  		case "char":
  			$("#filterComparison").html("\
                <option value=\"" + comparison.exact + "\">exact</option>\
  				");
  			break;
  		case "number":
  			$("#filterComparison").html("\
                <option value=\"" + comparison.exact + "\">exact</option>\
                <option value=\"" + comparison.lessThan + "\">less than</option>\
                <option value=\"" + comparison.greaterThan + "\">greater than</option>\
  				");
  			break;
  	}
    if (prevSearchType == "")
      prevSearchType = type;
    if (prevSearchType != type) {
      prevSearchType = type;
      $("#keyword").val("");
    }
  });
  $("#searchForm").submit(function(e) {
    e.preventDefault();
    searchDevices($("#keyword").val());
  });
});

function searchDevices(keyword) {
  var originalKeyword = keyword;
  var Device = Parse.Object.extend("Device");
  var query = new Parse.Query(Device);
  var field = $("#filterField").val();
  var comparisonIdx = parseInt($("#filterComparison").val());
  switch ($("#filterField option:selected").attr("type")) {
    case "number":
      keyword = parseInt(keyword);
      break;
    case "date":
      keyword = new Date(keyword);
      break;
    case "string":
      field += "_toUpperCase";
      keyword = keyword.toUpperCase();
      break;
  }
  switch (comparisonIdx) {
    case comparison.contains:
      query.contains(field, keyword);
      break;
    case comparison.exact:
      query.equalTo(field, keyword);
      break;
    case comparison.startsWith:
      query.startsWith(field, keyword);
      break;
    case comparison.before:
    case comparison.lessThan:
      query.lessThan(field, keyword);
      break;
    case comparison.after:
    case comparison.greaterThan:
      query.greaterThan(field, keyword);
      break;
  }
  var type = $("#filterType").val();
  var typeDescription = "all";
  if (type != "") {
    query.contains("type", type);
    typeDescription = type == "L" ? "laptop" : "desktop";
  }
  $("#resultDescription").html("Results " + comparisonDescription[comparisonIdx - 1] + " "
    + "\"" + originalKeyword + "\" in " + $("#filterField option:selected").html() + " for "
    + typeDescription + " devices.");
  $("#ulSearchResults").html("");
  query.find({
    success: function(results) {
      if (results.length == 0) {
        $("#pnlNoResults").show();
      } else {
        var li = "";
        for (var i = 0; i < results.length; i++) {
          var result = results[i];
          var fieldValue = eval("result.attributes." + field.replace("_toUpperCase", ""));
          li += "\
          <li>\
            <h3><a href=\"detail.html?id=" + result.id + "&src=search\">" + result.attributes.compName + "</a></h3>\
            <small>" + $("#filterField option:selected").html() + ": " + fieldValue.getWordHighlighted(keyword) + "</small>\
          </li>\
          ";
        }
        $("#ulSearchResults").html(li);

        $("#pnlNoResults").hide();
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

String.prototype.getWordHighlighted = function(keyword) {
  var idx = this.indexOf(keyword);
  return this.substring(0, idx)
    + "<span style=\"color: red;\">"
    + this.substring(idx, idx + keyword.length + 1)
    + "</span>"
    + this.substring(idx + keyword.length + 1);
}

Date.prototype.getWordHighlighted = function(keyword) {
  return this.toString();
}

Number.prototype.getWordHighlighted = function(keyword) {
  return this.toString();
}