$(function() {
	setupFormValidation();
	$("#passwordForm").submit(function(e) {
		e.preventDefault();
		if ($("#passwordForm").valid()) {
			var email = $("#pwdEmail").val().toLowerCase().trim();
			Parse.User.requestPasswordReset(email, {
				success: function() {
					alert('We have sent you an email with further instructions on resetting your password.  After password reset, login here.');
					top.location.href = "index.html";
				},
				error: function(error) {
					alert("We did not find an account with this email.");
					LogException("Error: " + error.code + " " + error.message);
				}
			});
		}
	});
	$("#usernameForm").submit(function(e) {
		e.preventDefault();
		if ($("#usernameForm").valid()) {
			alert('We have sent you an email with your username.  After receiving username, login here.');
			top.location.href = "index.html";
		}
	});
	$("#activateForm").submit(function(e) {
		e.preventDefault();
		if ($("#activateForm").valid()) {
			var user = Parse.Object.extend("User");
			var query = new Parse.Query(user);
			var email = $("#actEmail").val().toLowerCase().trim();
			query.equalTo("email", email);
			query.first({
				success: function(object) {
					object.set("email", email);
					object.save();
					alert('We have sent you an email to activate your account.  After activation login here.');
					top.location.href = "index.html";
				},
				error: function(error) {
					alert("We did not find an account with this email.");
					LogException("Error: " + error.code + " " + error.message);
				}
			});
		}
	});
});

function setupFormValidation() {
	$("#passwordForm").validate({
		rules: {
			pwdEmail: {
				required: true,
				email: true
			}
		},
		messages: {
			pwdEmail: {
				required: "Please enter an email",
				email: "Please provide a valid email"
			}
		}
	});
	$("#usernameForm").validate({
		rules: {
			usrEmail: {
				required: true,
				email: true
			}
		},
		messages: {
			usrEmail: {
				required: "Please enter an email",
				email: "Please provide a valid email"
			}
		}
	});
	$("#activateForm").validate({
		rules: {
			actEmail: {
				required: true,
				email: true
			}
		},
		messages: {
			actEmail: {
				required: "Please enter an email",
				email: "Please provide a valid email"
			}
		}
	});
}