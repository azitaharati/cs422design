$(function() {
	setupFormValidation();
	$("#purchased").datepicker();
	$("#addForm").submit(function(e) {
		e.preventDefault();

		if ($("#addForm").valid()) {
			var Device = Parse.Object.extend("Device");			
			var device = new Device();
			device.save({
				compName: $("#compName").val().toUpperCase().trim(),
				compName_toUpperCase: $("#compName").val().toUpperCase().trim(),
				serialNo: $("#serialNo").val().toUpperCase().trim(),
				serialNo_toUpperCase: $("#serialNo").val().toUpperCase().trim(),
				ptag: $("#ptag").val().toUpperCase().trim(),
				ptag_toUpperCase: $("#ptag").val().toUpperCase().trim(),
				userFirstName: $("#userFirstName").val().trim(),
				userFirstName_toUpperCase: $("#userFirstName").val().toUpperCase().trim(),
				userLastName: $("#userLastName").val().trim(),
				userLastName_toUpperCase: $("#userLastName").val().toUpperCase().trim(),
				location: $("#location").val(),
				location_toUpperCase: $("#location").val().toUpperCase().trim(),
				model: $("#model").val().trim(),
				model_toUpperCase: $("#model").val().trim().toUpperCase().trim(),
				purchased: new Date($("#purchased").val()),
				age: parseInt($("#age").val()),
				psh: $("#psh").val().toUpperCase(),
				psh_toUpperCase: $("#psh").val().toUpperCase(),
				dept: $("#dept").val().toUpperCase(),
				dept_toUpperCase: $("#dept").val().toUpperCase(),
				cc: $("#cc").val().toUpperCase(),
				cc_toUpperCase: $("#cc").val().toUpperCase(),
				type: $("#type").val().toUpperCase(),
			}, {
				success: function(device) {
					redirectTo("detail.html?id=" + device.id);
				},
				error: function(device, error) {
					alert('Failed to create new object, with error code: ' + error.message);
				}
			});
		}
	});
});

function setupFormValidation() {
	$("#addForm").validate({
		rules: {
			compName: {
				required: true,
				rangelength: [6, 20]
			},
			serialNo: {
				required: true,
				rangelength: [4, 15]
			},
			ptag: {
				required: false
			},
			userFirstName: {
				required: true,
				rangelength: [2, 20]
			},
			userLastName: {
				required: true,
				rangelength: [2, 20]
			},
			location: {
				required: true
			},
			model: {
				required: true,
				rangelength: [2, 15]
			},
			purchased: {
				required: true
			},
			age: {
				required: true
			},
			psh: {
				required: true
			},
			dept: {
				required: true
			},
			cc: {
				required: true
			},
			type: {
				required: true
			}
		},
		messages: {
			compName: {
				required: "Please enter computer name",
				rangelength: "Computer name must be between 6 and 20 characters"
			},
			email: {
				required: "Please enter serial #",
				rangelength: "Computer name must be between 4 and 15 characters"
			},
			userFirstName: {
				required: "Please enter user's first name",
				rangelength: "User's first name must be between 2 and 20 characters"
			},
			userLastName: {
				required: "Please enter user's last name",
				rangelength: "User's last name must be between 2 and 20 characters"
			},
			location: {
				required: "Please select a location"
			},
			model: {
				required: "Please enter a model name",
				rangelength: "Model name must be between 2 and 15 characters"
			},
			purchased: {
				required: "Please enter a purchased date"
			},
			age: {
				required: "Please enter age"
			},
			psh: {
				required: "Please select P, S, or H"
			},
			dept: {
				required: "Please select department"
			},
			cc: {
				required: "Please select CC"
			},
			type: {
				required: "Please select device type"
			}
		}
	});
}