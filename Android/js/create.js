$(function() {
	setupFormValidation();
	$("#createForm").submit(function(e) {
		e.preventDefault();

		if ($("#createForm").valid()) {
			var username = $("#username").val().toLowerCase().trim();
			var email = $("#email").val().toLowerCase().trim();
			var password = $("#password").val();
			var confirmPassword = $("#confirmPassword").val();

			var User = Parse.Object.extend("User");
			var query = new Parse.Query(User);
			query.equalTo("username", username);
			query.find({
				success: function(results) {
					if (results.length != 0) {
						alert("Username already exists.  Try another username.");
					} else {
						var user = new User();
						user.set("username", username);
						user.set("email", email);
						user.set("password", password);
						user.signUp(null, {
							success: function(user) {
								alert('We have sent you an email on how to activate your account.  After activation login here.');
								top.location.href = "index.html";
							},
							error: function(user, error) {
								alert('Failed to create new user.  Please try again later.');
								LogException('Failed to create new user, with error code: ' + error.message);
							}
						});
					}
				},
				error: function(error) {
					alert("Error: " + error.code + " " + error.message);
				}
			});
		}
	});
});

function setupFormValidation() {
	$("#createForm").validate({
		rules: {
			username: {
				required: true,
				rangelength: [4, 15]
			},
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				rangelength: [6, 20]
			},
			confirmPassword: {
				required: true,
				rangelength: [6, 20],
				equalTo: "#password"
			}
		},
		messages: {
			username: {
				required: "Please enter a username",
				rangelength: "Username must be between 4 and 15 characters"
			},
			email: {
				required: "Please enter an email",
				email: "Please provide a valid email"
			},
			password: {
				required: "Please enter a password",
				rangelength: "Passwords must be between 6 and 20 characters"
			},
			confirmPassword: {
				required: "Please confirm your password",
				rangelength: "Passwords must be between 6 and 20 characters",
				equalTo: "Passwords entered do not match"
			}
		}
	});
}