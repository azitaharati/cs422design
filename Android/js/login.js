$(function() {
	$("#loginForm").submit(function(e) {
		e.preventDefault();

		if (isFormValid()) {
			var username = $("#username").val().toLowerCase().trim();
			var password = $("#password").val();
			Parse.User.logIn(username, password, {
				success: function(user) {
					if (user.attributes.emailVerified) {
						window.localStorage.setItem("username", username);
						top.location.href = "view.html";
					} else {
						alert("Your account still requires activation.  We had sent you instructions on how to activate your account.  If you did not receive this email, then go to 'Can't access your account?'");
					}
				},
				error: function(user, error) {
					alert("Invalid username or password. Please try again.");
				}
			});
		}
	});
});

function isFormValid() {
	return $("#loginForm").validate();
}