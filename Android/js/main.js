
Parse.initialize("urV0ccxaYD06L5MtOG32TpGtEKa7qbYSNiA5SjFZ", "4IivjHaFoG8olRPSddR56PIjC4mzyLB7zwH6oetf");
var isLoggedIn = false;

if (!window.navigator.standalone) {
	document.addEventListener("DOMContentLoaded", adjustHeight, false);
} 
else {
	document.addEventListener("click", clickHandler, true);
}

var isMobileDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

if (!isMobileDevice) {
	$('head').append('<link rel="stylesheet" type="text/css" href="css/main_desktop.css">');
}

function adjustHeight() {
	var html = document.documentElement;
	var size = window.innerHeight;

	html.style.height = (size + size) + "px";
	window.setTimeout(function() {
		if (window.pageYOffset == 0) {
			window.scrollTo(0, 0);
		}
		html.style.height = window.innerHeight + "px";
	}, 0);
}

/* Outgoing links */
function link(anchor) {
	window.location.href=anchor.href;
	return false;
}

/* Orientation LANDSCAPE/PORTRAIT */
function Orientation() {

	var contentType = "";
	switch(window.orientation) {

		case 0:
		contentType += "portrait";
		break;

		case -90:
		contentType += "landscape";
		break;

		case 90:
		contentType += "landscape";
		break;

		case 180:
		contentType += "portrait";
		break;
	
	}
	document.getElementById("wrapper").setAttribute("class", contentType);
}

function LogException(e) {
    if (window.console // check for window.console not console
         && window.console.log) window.console.log(e);
    // Other logging here
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var path = window.location.pathname;
var page = path.split("/").pop();

$(function() {

	// this logic copies Android activity stack logic
	// so pressing device back button show expected behavior

	var pgCnt = window.localStorage.getItem("pgCnt");
	if (pgCnt === undefined || pgCnt == null || pgCnt.length == 0)
		pgCnt = 1;
	else
		pgCnt = parseInt(pgCnt);
	
	var pages = window.localStorage.getItem("pages");
	if (pages === undefined || pages == null || pages.length == 0)
		pages = "{}";
	pages = JSON.parse(pages);
	var pageIdx = eval("pages." + page.replace(".", ""));
	if (pageIdx === undefined || pageIdx == null || pageIdx.length == 0)
		pageIdx = pgCnt;
	else
		pageIdx = parseInt(pageIdx);
	eval("pages." + page.replace(".", "") + " = pageIdx");
	$.each(pages, function(k, v) {
		if (parseInt(v) > pageIdx) {
			delete pages[k];
		}
	});
	var pages = JSON.stringify(pages);

	window.localStorage.setItem("pages", pages);
	window.localStorage.setItem("pgCnt", parseInt(pageIdx + 1));
	
	switch(page) {
		case "add.html":
		case "view.html":
		case "detail.html":
		case "search.html":
		case "update.html":
			var username = window.localStorage.getItem("username");
			if (username === undefined || username == null || username.length == 0) {
				top.location.href = "index.html";
			} else {
				isLoggedIn = true;	
			}
			break;
		default:
			isLoggedIn = false;
			break;
	}
	
	loadTemplates();
	loadPageToolbar();
	
	$("#header").on("click", ".toggle", function() {
		$(this).next().slideToggle("fast");
		return false;
	});
	
	$("#header").on("click", "#signOut", function() {
		window.localStorage.removeItem("username");
	});
});

window.onload = function () {
	if (!isMobileDevice)
		return;
	
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            // Handle the back (or forward) buttons here
            // Will NOT handle refresh, use onbeforeunload for this.
            onBackButtonPressed();
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
                // Detect and redirect change here
                // Works in older FF and IE9
                // * it does mess with your hash symbol (anchor?) pound sign
                // delimiter on the end of the URL
				onBackButtonPressed();
            }
            else {
                ignoreHashChange = false;   
            }
        };
    }
}

function onBackButtonPressed() {
	if (page != "index.html" && page != "access.html" && page != "create.html" && page != "signout.html") {
		if (confirm("Are you sure you want to sign out?")) {
            $("#signOut").click();
        }
	} else if (page != "index.html") {
		top.location.href = "index.html";
	} else {
		if (confirm("Are you sure you want to close this app?")) {
            navigator.app.exitApp();
        }
	}
}

function redirectTo(href) {
	var redirectLink = $("#redirectLink");
	if (redirectLink.length == 0) {
		alert("failed to redirect");
		return;
	}
	redirectLink.attr("href", href);
	redirectLink[0].click();
}

function loadTemplates() {
	if (isLoggedIn) {
		leftNav = "\
			<li>\
			  <a href=\"view.html\" class=\"stackTrace\">View</a>\
			</li>\
			<li>\
			  <a href=\"add.html\" class=\"stackTrace\">Add</a>\
			</li>\
			<li>\
			  <a href=\"search.html\" class=\"stackTrace\">Search</a>\
			</li>\
		";
	} else {
		leftNav = "\
			<li>\
			  <a href=\"index.html\" class=\"stackTrace\">Sign In</a>\
			</li>\
			<li>\
			  <a href=\"create.html\" class=\"stackTrace\">Sign Up</a>\
			</li>\
		";
	}
	var welcome = "";
	if (!isMobileDevice && isLoggedIn) {
		welcome = "<div id=\"welcomeHeader\">Hi, " + window.localStorage.getItem("username") + "</div>";
	}
	$("#header").html(
	"\
<a class=\"header-hamburger toggle\">&nbsp;</a> \
<div id=\"leftnav\">\
  <ul>" + leftNav + "</ul>\
</div>\
<h1 id=\"logo\" style=\"left: 42px;\">\
  <a href=\"" + (isLoggedIn ? "view.html" : "index.html") + "\">UIC AITS Inventory</a>\
</h1>" + welcome + "\
<a class=\"header-button toggle\" " + (!isLoggedIn ? "style=\"display: none\"" : "") + ">&nbsp;</a> \
<div id=\"rightnav\">\
  <ul>\
	<li>\
	  <a id=\"signOut\" href=\"signout.html\" onclick=\"return link(this)\">Log Out</a>\
	</li>\
  </ul>\
</div>\
	"
	);
	
	$("#footer").html(
	"<p>Copyright disclaimer</p>"
	);
}

function loadPageToolbar() {

	var editBtn = "";
	var deleteBtn = "";
	var backBtn = "";

	switch(page) {
		case "detail.html":
			editBtn = "<button id=\"editBtn\">Edit</button>";
			deleteBtn = "<button id=\"deleteBtn\">Delete</button>";
			break;
	}
	
	var referrer = "";
	switch(page) {
		case "detail.html":
			referrer = (getParameterByName("src") == "search") ? "search.html" : "view.html";
			break;
		case "update.html":
			referrer = "detail.html?id=" + getParameterByName("id");
			break;
	}
	if (referrer.length > 0)
		backBtn = "<button id=\"backBtn\" onclick=\"top.location.href='" + referrer + "';\"\">Back</button>";
	
	$("#pagetoolbar").html(editBtn + deleteBtn + backBtn);
}

// extentions

Number.prototype.toString = function(mask) {
	if (mask === undefined)
		return this + "";
	var d = this + "";
	var result = "";
	for(var i = mask.length - 1; i >= 0; i--) {
		result += (i > (d.length - 1) ? mask[i] : d[d.length - 1 - i]);
	}
	return result;
}

Date.prototype.toString = function() {
	var result = (this.getMonth() + 1).toString("00") + "/"
		+ this.getDate().toString("00") + "/"
		+ this.getFullYear();
	return result;
}